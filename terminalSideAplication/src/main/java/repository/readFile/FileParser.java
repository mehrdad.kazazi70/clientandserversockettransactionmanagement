package repository.readFile;

import model.entity.OutLog;
import model.entity.Server;
import model.entity.Terminal;
import model.entity.Transaction;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The input file is decomposed using parser and quantified in objects like them
 *
 * @return The object of terminal in XMLFile
 */

public class FileParser {

    public static Terminal getXMLElements(String fileAddress) {
        Terminal terminal = new Terminal();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        Document parse = null;
        try {
            builder = factory.newDocumentBuilder();
            parse = builder.parse(new File(fileAddress));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Element documentElement = parse.getDocumentElement();
        terminal.setId(documentElement.getAttribute("id"));
        terminal.setType(documentElement.getAttribute("type"));
        terminal.setServer(new Server((parse.getElementsByTagName("server").item(0).getAttributes().getNamedItem("ip").getTextContent()), (parse.getElementsByTagName("server").item(0).getAttributes().getNamedItem("port").getTextContent())));
        terminal.setOutLog(new OutLog((parse.getElementsByTagName("outLog").item(0).getAttributes().getNamedItem("path").getTextContent())));
        terminal.setTransactions(getTransactionFromFile(parse));
        return terminal;
    }

    private static List<Transaction> getTransactionFromFile(Document parse) {
        List<Transaction> transactionList = new ArrayList<Transaction>();
        NodeList transactionsNode = parse.getElementsByTagName("transaction");
        for (int i = 0; i < transactionsNode.getLength(); i++) {
            Transaction transaction = new Transaction();
            transaction.setId(transactionsNode.item(i).getAttributes().getNamedItem("id").getTextContent());
            transaction.setType(transactionsNode.item(i).getAttributes().getNamedItem("type").getTextContent());
            transaction.setAmount(transactionsNode.item(i).getAttributes().getNamedItem("amount").getTextContent());
            transaction.setDeposit(transactionsNode.item(i).getAttributes().getNamedItem("deposit").getTextContent());
            transactionList.add(transaction);
        }
        return transactionList;
    }
}

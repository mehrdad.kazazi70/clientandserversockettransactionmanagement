package service.socket;

import model.entity.FinalResult;
import model.entity.Terminal;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;

/**
 * Using the socket, the terminal sends the data to the server and waits for a response from the server
 */
public class ClientSocket {
    private static final Logger LOGGER = Logger.getLogger(ClientSocket.class);

    public static List<FinalResult> socketHandler(Terminal terminal) {
        try {
            Socket socket = new Socket(terminal.getServer().getIp(), Integer.parseInt(terminal.getServer().getPort()));
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(terminal.getTransactions());
            LOGGER.info("reports of transactions retrieve from server side.");
            ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
            List<FinalResult> finalResultList= (List<FinalResult>) objectInputStream.readObject();
            for (FinalResult finalResult : finalResultList) {
                LOGGER.info("CustomerId:"+" "+finalResult.getId() +" has "+"("+ finalResult.getStatus()+")"+" status , also Amount is :"+finalResult.getAmount()+" after transactions");
            }
            socket.close();
            return finalResultList;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}

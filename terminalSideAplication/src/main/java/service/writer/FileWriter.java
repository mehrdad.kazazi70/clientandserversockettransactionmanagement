package service.writer;

import model.entity.FinalResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

/**
 * Documentation will take place and will be saved in the XML file
 */
public class FileWriter {
    private static final String XMLPath = "src/main/resources/outputReport/response.xml";

    public static void writeReport(List<FinalResult> finalResultList) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            Element rootElement = document.createElement("ReportTransactions");
            document.appendChild(rootElement);
            for (FinalResult finalResult : finalResultList) {
                rootElement.appendChild(createChildElement(document, finalResult));
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(XMLPath));
            transformer.transform(domSource, streamResult);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    private static Node createChildElement(Document document, FinalResult finalResult) {
        Element element = document.createElement("ReportTransaction");
        element.setAttribute("id", finalResult.getId());
        element.appendChild(createChildElements(document, element, "status", String.valueOf(finalResult.getStatus())));
        element.appendChild(createChildElements(document, element, "amount", finalResult.getAmount()));
        return element;
    }

    private static Node createChildElements(Document document, Element element, String status, String amount) {
        Element node = document.createElement(status);
        node.appendChild(document.createTextNode(amount));
        return node;
    }
}

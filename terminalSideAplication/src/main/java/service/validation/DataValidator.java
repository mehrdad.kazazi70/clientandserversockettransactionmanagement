package service.validation;

import model.entity.Terminal;
import model.entity.Transaction;
import org.apache.log4j.Logger;

/**
 * All values are written in the file after validation
 * This report will be recorded by the logging class
 *
 * @return The result is returned as a boolean
 */

public class DataValidator {
    private static final Logger LOGGER = Logger.getLogger(DataValidator.class);

    private static boolean isNull(String value) {
        return value != null;
    }

    private static boolean isBlank(String value) {
        return value != null;
    }

    private static boolean isEmpty(String value) {
        if (isNull(value))
            return !value.isEmpty();
        return false;
    }

    private static boolean isNegative(String value) {
        return Integer.valueOf(value) > 0;
    }

    public static boolean log(Terminal terminal) {
        if (!DataValidator.isNull(terminal.getId()) || !DataValidator.isBlank(terminal.getId())) {
            LOGGER.error("ID is invalid in request");
            return false;
        }
        if (!DataValidator.isNull(terminal.getType()) || !DataValidator.isBlank(terminal.getType())) {
            LOGGER.error("Type is invalid in request");
            return false;
        }
        if (!DataValidator.isNull(terminal.getServer().getIp()) || !DataValidator.isBlank(terminal.getServer().getIp())) {
            LOGGER.error("IP is invalid in request");
            return false;
        }
        if (!DataValidator.isNull(terminal.getServer().getPort()) || !DataValidator.isBlank(terminal.getServer().getPort())) {
            LOGGER.error("Port is invalid in request");
            return false;
        }
        if (!DataValidator.isNull(terminal.getOutLog().getPath()) || !DataValidator.isBlank(terminal.getOutLog().getPath())) {
            LOGGER.error("Path is invalid in request");
            return false;
        }
        for (Transaction transactionList : terminal.getTransactions()) {
            if (!DataValidator.isNull(transactionList.getId()) || !DataValidator.isEmpty(transactionList.getId().trim())) {
                LOGGER.error("ID for " + transactionList.getDeposit() + " is invalid in request");
                return false;
            }
            if (!DataValidator.isNull(transactionList.getType()) || !DataValidator.isEmpty(transactionList.getType().trim())) {
                LOGGER.error("Type for " + transactionList.getDeposit() + " is invalid in request");
                return false;
            }
            if (!DataValidator.isNull(transactionList.getAmount()) || !DataValidator.isEmpty(transactionList.getAmount().trim())) {
                LOGGER.error("Amount for " + transactionList.getAmount() + " is invalid in request");
                return false;
            }
            if (!DataValidator.isNull(transactionList.getDeposit()) || !DataValidator.isEmpty(transactionList.getDeposit().trim())) {
                LOGGER.error("Deposit for " + transactionList.getDeposit() + " is invalid in request");
                return false;
            }
        }
        if (!DataValidator.isEmpty(terminal.getId().trim())) {
            LOGGER.error("id is Empty in request");
            return false;
        }
        if (!DataValidator.isEmpty(terminal.getType().trim())) {
            LOGGER.error("Type is empty in request");
            return false;
        }
        if (!DataValidator.isEmpty(terminal.getServer().getIp().trim())) {
            LOGGER.error("IP is empty in request");
            return false;
        }
        if (!DataValidator.isEmpty(terminal.getServer().getPort().trim())) {
            LOGGER.error("Port is empty in request");
            return false;
        }
        for (Transaction transactionList : terminal.getTransactions()) {
            if (!DataValidator.isNegative(transactionList.getAmount().trim())) {
                LOGGER.error("Amount for" + transactionList.getDeposit() + " is unknown value in request");
                return false;
            }
        }
        if (!DataValidator.isEmpty(terminal.getOutLog().getPath().trim())) {
            LOGGER.error("Path is empty in request");
            return false;
        } else {
            LOGGER.info("request sent from client to server, all of the validations are ok!");
            return true;
        }
    }
}

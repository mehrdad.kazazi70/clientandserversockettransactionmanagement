package model.entity;

import java.io.Serializable;
import java.util.List;

public class Terminal implements Serializable {
    private String id;
    private String type;
    private Server server;
    private OutLog outLog;
    private List<Transaction> transactions;

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public OutLog getOutLog() {
        return outLog;
    }

    public void setOutLog(OutLog outLog) {
        this.outLog = outLog;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

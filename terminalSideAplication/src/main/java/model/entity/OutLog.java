package model.entity;


public class OutLog {
    private String path;

    public OutLog(String path) {
        this.path = path;
        setPath(path);
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}

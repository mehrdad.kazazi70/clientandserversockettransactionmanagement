package model.entity;

public class Server {
    private String ip;
    private String port;

    public Server(String ip, String port) {
        this.ip = ip;
        setIp(ip);
        this.port = port;
        setPort(port);
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}

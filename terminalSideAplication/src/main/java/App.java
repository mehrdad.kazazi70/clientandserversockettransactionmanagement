import model.entity.FinalResult;
import model.entity.Terminal;
import org.apache.log4j.Logger;
import repository.readFile.FileParser;
import service.socket.ClientSocket;
import service.validation.DataValidator;
import service.writer.FileWriter;

import java.util.List;

/**
 * The class get input file then process in there and after write log send for server
 * receive data and write documentary and giveback report to client in xml file
 *
 * @author Mehrdad Kazazi
 */

public class App {
    private static final Logger LOGGER = Logger.getLogger(App.class);
    public static void main(String[] args) {
        String fileAddress = "src/main/resources/input/InputFile.xml";
        Terminal terminal = FileParser.getXMLElements(fileAddress);
        if (DataValidator.log(terminal)) {
            List<FinalResult> finalResultList = ClientSocket.socketHandler(terminal);
            FileWriter.writeReport(finalResultList);


        } else {
            LOGGER.error("An error has occurred , received Data has problem ");
        }
    }
}

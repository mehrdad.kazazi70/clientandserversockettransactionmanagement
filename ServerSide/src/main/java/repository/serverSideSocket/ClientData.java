package repository.serverSideSocket;

import model.entity.Transaction;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Objects related to all terminal requests are locked here
 */
public class ClientData {

    public static List<Transaction> getData(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
      return (List<Transaction>) objectInputStream.readObject();

    }
}

package repository.serverSideSocket;

/**
 * An enum structure has been used to prevent unintentional changes
 */
public enum PortConfig {
    PORT(8080);
    private int value;

    PortConfig(int value) {
        this.value = value;
    }
    public int getValue(){
       return value;
    }
}

package repository.serverSideSocket;


import model.entity.AccountInformation;
import model.entity.Deposit;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * The File that contains the server side settings as well as customer information is decomposed
 * It is poured into the corresponding fields
 *
 * @return
 */
public class FileParser {
    public static AccountInformation getSettingFile(String fileAddress) {
        AccountInformation accountInformation = new AccountInformation();
        try {
            JSONParser jsonParser = new JSONParser();
            Object object = jsonParser.parse(new FileReader(fileAddress));
            JSONObject jsonObject = (JSONObject) object;
            String portOfTerminal = (String) jsonObject.get("port");
            accountInformation.setPort(portOfTerminal);
            String outLogOfTerminal = (String) jsonObject.get("outLog");
            accountInformation.setOutLog(outLogOfTerminal);
            JSONArray jsonArray = (JSONArray) jsonObject.get("deposits");
            List<Deposit> depositList = new ArrayList<>();
            for (int i = 0; i < jsonArray.size(); i++) {
                Deposit deposit = new Deposit();
                JSONObject jsonObject1 = (JSONObject) jsonArray.get(i);
                deposit.setId(jsonObject1.get("id").toString());
                deposit.setCustomer(jsonObject1.get("customer").toString());
                deposit.setInitialBalance(jsonObject1.get("initialBalance").toString());
                deposit.setUpperBound(jsonObject1.get("upperBound").toString());
                depositList.add(deposit);
            }
            accountInformation.setDeposits(depositList);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return accountInformation;
    }
}

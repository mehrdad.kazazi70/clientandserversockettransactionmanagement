import model.entity.AccountInformation;
import model.entity.FinalResult;
import model.entity.Transaction;
import org.apache.log4j.Logger;
import repository.serverSideSocket.ClientData;
import repository.serverSideSocket.FileParser;
import repository.serverSideSocket.PortConfig;
import service.Writer.SendResponse;
import service.business.process.Processor;
import service.validation.LogReporter;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

/**
 * The information is taken from the terminal
 * Accounting changes and control of values and validation are performed on them
 * The answer is finally returned to the terminal
 */
public class App {
    private static final Logger LOGGER = Logger.getLogger(App.class);

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        final String fileAddress = "D:\\git\\Dotin Project 2\\ServerSide\\src\\main\\resources\\core.json";
        final AccountInformation accountInformation = FileParser.getSettingFile(fileAddress);
        ServerSocket serverSocket = new ServerSocket(PortConfig.PORT.getValue());
        while (true) {
            LOGGER.info("socket for receive request data from client has opened");
            final Socket socket = serverSocket.accept();
            if (LogReporter.log(accountInformation)) {
                final ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
                final ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream());
                LOGGER.info("Request data received from client to the server");
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        try {
                            List<Transaction> transactionList = ClientData.getData(objectInputStream);
                            List<FinalResult> reportTransaction = Processor.getInstance().process(accountInformation.getDeposits(), transactionList, fileAddress);
                            SendResponse sendResponse = new SendResponse();
                            sendResponse.send(objectOutputStream, reportTransaction);
                            LOGGER.info("All of the Transactions done and retrieve to client side .");
                        } catch (IOException | ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
                try {
                    thread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else
                LOGGER.warn("An error has occurred !  ");
            socket.close();
        }
    }
}

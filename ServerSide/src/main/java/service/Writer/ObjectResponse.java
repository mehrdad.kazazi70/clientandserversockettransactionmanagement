package service.Writer;

import model.entity.FinalResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Object values are created to be sent to the terminal
 */

public class ObjectResponse {
    public static List<Object> ObjectResponse(FinalResult finalResult){
        List<Object> responseList = new ArrayList<>();
        responseList.add(finalResult.getStatus());
        responseList.add(finalResult.getAmount());
        return responseList;
    }
}

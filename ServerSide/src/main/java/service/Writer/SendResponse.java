package service.Writer;

import model.entity.FinalResult;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * Under all circumstances, reports to the terminal are prepared using this method
 */
public class SendResponse {
    public void send(ObjectOutputStream objectOutputStream,List<FinalResult> finalResultList) throws IOException {
       objectOutputStream.writeObject(finalResultList);
    }

}

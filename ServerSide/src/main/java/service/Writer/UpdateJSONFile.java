package service.Writer;

import model.entity.AccountInformation;
import model.entity.Deposit;
import model.entity.FinalResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import repository.serverSideSocket.FileParser;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * New values are entered into the Jason File
 */
public class UpdateJSONFile {
    public void updateJSON(Deposit deposit, String address, String finalAmount) throws IOException {
        AccountInformation accountInformation = FileParser.getSettingFile(address);
        FinalResult finalResult = new FinalResult();
        List<Deposit> depositList = accountInformation.getDeposits();
        for (Deposit deposit1 : depositList) {
            if (deposit1.getId().equals(deposit.getId())) {
                deposit1.setUpperBound(finalAmount);
            }
        }
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        ObjectResponse sendObject = new ObjectResponse();
        for (int i = 0; i < accountInformation.getDeposits().size(); i++) {
            JSONObject jsonObject1 = new JSONObject();
            jsonObject1.put("customer", accountInformation.getDeposits().get(i).getCustomer());
            jsonObject1.put("id", accountInformation.getDeposits().get(i).getId());
            jsonObject1.put("initialBalance", accountInformation.getDeposits().get(i).getInitialBalance());
            jsonObject1.put("upperBound", accountInformation.getDeposits().get(i).getUpperBound());
            jsonArray.add(jsonObject1);
        }
        jsonObject.put("port", accountInformation.getPort());
        jsonObject.put("outLog", accountInformation.getOutLog());
        jsonObject.put("deposits", jsonArray);
        FileWriter fileWriter = new FileWriter(address);
        fileWriter.write(jsonObject.toJSONString());
        fileWriter.flush();
        fileWriter.close();
    }
}

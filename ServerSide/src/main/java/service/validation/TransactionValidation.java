package service.validation;

import model.entity.Transaction;

/**
 * The input values in the terminal used for the calculations are checked at this stage
 */
public class TransactionValidation {
    public static boolean isPositive(Transaction transaction) {
        return Integer.parseInt(transaction.getAmount()) >= 0;
    }
}

package service.validation;

import model.entity.AccountInformation;
import model.entity.Deposit;
import org.apache.log4j.Logger;

/**
 * All values are written in the File after validation
 * This report will be recorded by the logging class
 *
 * @return The result is returned as a boolean
 */
public class LogReporter {
    private static final Logger LOGGER = Logger.getLogger(LogReporter.class);

    private static boolean isNull(String value) {
        return value != null;
    }

    private static boolean isEmpty(String value) {
        if (isNull(value))
            return !value.isEmpty();
        return false;
    }

    public static boolean log(AccountInformation accountInformation) {
        if (!LogReporter.isEmpty(accountInformation.getPort())) {
            LOGGER.error("port is invalid");
            return false;
        }
        for (Deposit deposits : accountInformation.getDeposits()) {
            if (!LogReporter.isEmpty(deposits.getId().trim()) || !LogReporter.isEmpty(deposits.getCustomer().trim()) || !LogReporter.isEmpty(deposits.getInitialBalance().trim()) || !LogReporter.isEmpty(deposits.getUpperBound().trim())) {
                LOGGER.error("deposit has invalid field");
                return false;
            }
        }
        if (!LogReporter.isEmpty(accountInformation.getOutLog())) {
            LOGGER.error("outLog is invalid");
            return false;
        }
        return true;
    }
}

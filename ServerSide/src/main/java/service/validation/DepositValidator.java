package service.validation;

import model.entity.Deposit;

/**
 * The input values are checked at this stage
 */
public class DepositValidator {
    public static boolean isPositive(Deposit deposit) {
        return Integer.valueOf(deposit.getInitialBalance()) >= 0 && Integer.valueOf(deposit.getUpperBound()) >= 0;
    }
}

package service.business.process;

import model.entity.Deposit;
import model.entity.FinalResult;
import model.entity.Transaction;

import java.io.IOException;

public interface Calculator {
    FinalResult calculate(Deposit deposit, Transaction transaction, String fileAddress) throws IOException;
}

package service.business.process;

import model.entity.Deposit;
import model.entity.FinalResult;
import model.entity.Transaction;
import org.apache.log4j.Logger;
import service.Writer.UpdateJSONFile;
import service.validation.DepositValidator;
import service.validation.TransactionValidation;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Requests for an account increase have been calculated and evaluated in this section
 */
public class DepositService implements Calculator {
    private static final Logger LOGGER = Logger.getLogger(DepositService.class);
    @Override
    public FinalResult calculate(Deposit deposit, Transaction transaction, String address) throws IOException {
        String result = null;
        FinalResult finalResult = new FinalResult();
        if (DepositValidator.isPositive(deposit) && TransactionValidation.isPositive(transaction)) {
            result = String.valueOf(castToBigDecimal(transaction.getAmount()).add(castToBigDecimal(deposit.getInitialBalance())).add(castToBigDecimal(deposit.getUpperBound())));
        }
        if (!result.equals("0")) {
            UpdateJSONFile updateJSONFile = new UpdateJSONFile();
            updateJSONFile.updateJSON(deposit, address, String.valueOf(castToBigDecimal(result).subtract(castToBigDecimal(deposit.getInitialBalance()))));
            finalResult.setAmount(String.valueOf(castToBigDecimal(result).subtract(castToBigDecimal(deposit.getInitialBalance()))));
            finalResult.setStatus(true);
            finalResult.setId(deposit.getId());
            LOGGER.info(deposit.getId() + "  was succeed,Amount is : " + finalResult.getAmount());
            return finalResult;
        } else
            LOGGER.error(deposit.getId() + " was failed");
            finalResult.setId(deposit.getId());
            finalResult.setStatus(false);
            String validValue=String.valueOf(castToBigDecimal(deposit.getInitialBalance()).add(castToBigDecimal(deposit.getUpperBound())));
            finalResult.setAmount(validValue);
            return finalResult;
    }

    private BigDecimal castToBigDecimal(String s) {
        return new BigDecimal(s);
    }


}

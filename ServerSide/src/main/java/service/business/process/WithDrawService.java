package service.business.process;

import model.entity.Deposit;
import model.entity.FinalResult;
import model.entity.Transaction;
import service.Writer.UpdateJSONFile;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Requests for an account decries have been calculated and evaluated in this section
 */
public class WithDrawService implements Calculator {
    @Override
    public FinalResult calculate(Deposit deposit, Transaction transaction, String address) throws IOException {

        FinalResult finalResult = new FinalResult();
        String result = String.valueOf(castToBigDecimal(deposit.getInitialBalance()).add(castToBigDecimal(deposit.getUpperBound())).subtract(castToBigDecimal(transaction.getAmount())));
        if (result.equals("0")) {
            UpdateJSONFile updateJSONFile = new UpdateJSONFile();
            updateJSONFile.updateJSON(deposit, address, String.valueOf(castToBigDecimal(result).subtract(castToBigDecimal(deposit.getInitialBalance()))));
            finalResult.setAmount(String.valueOf(castToBigDecimal(result).subtract(castToBigDecimal(deposit.getInitialBalance()))));
            finalResult.setId(deposit.getId());
            finalResult.setStatus(true);
        } else
            finalResult.setId(deposit.getId());
        finalResult.setStatus(false);
        String validValue = String.valueOf(castToBigDecimal(deposit.getInitialBalance()).add(castToBigDecimal(deposit.getUpperBound())));
        finalResult.setAmount(validValue);
        return finalResult;
    }

    private BigDecimal castToBigDecimal(String s) {
        return new BigDecimal(s);
    }
}

package service.business.process;

import model.entity.Deposit;
import model.entity.FinalResult;
import model.entity.Transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Depending on the type of request, the calculation method is implemented
 */

public class Processor {
    private static Processor processor = new Processor();

    private Processor() {
    }

    public static Processor getInstance() {
        return processor;
    }

    public List<FinalResult> process(List<Deposit> deposits, List<Transaction> transactions, String address) throws IOException {
        synchronized (this) {
            List<FinalResult> finalResults = new ArrayList<>();
            for (Transaction transaction : transactions) {
                for (Deposit deposit : deposits) {
                    if (transaction.getDeposit().equals(deposit.getId())) {
                        switch (transaction.getType()) {
                            case "deposit":
                                DepositService depositService = new DepositService();
                                FinalResult depositsFinalResultList = depositService.calculate(deposit, transaction, address);
                                finalResults.add(depositsFinalResultList);
                                break;
                            case "withdraw":
                                WithDrawService withDrawService = new WithDrawService();
                                FinalResult withDrawsFinalResultList = withDrawService.calculate(deposit, transaction, address);
                                finalResults.add(withDrawsFinalResultList);
                                break;
                        }
                    }
                }
            }
            return finalResults;
        }
    }
}

